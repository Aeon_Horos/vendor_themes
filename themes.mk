# QS tile styles
PRODUCT_PACKAGES += \
    QStilesSquare \
    QStilesRoundedSquare \
    QStilesSquircle \
    QStilesTearDrop

# Themes
PRODUCT_PACKAGES += \
    DeskClockDark \
    DeskClockLight \
    ExactCalculatorDark \
    ExactCalculatorLight \
    GBoardDark \
    GBoardLight \
    GoogleIntelligenceSenseDark \
    GoogleIntelligenceSenseLight \
    MessagingDark \
    MessagingLight \
    SettingsDark \
    SettingsIntelligenceDark \
    SystemDark \
    SystemUiDark \
    WellbeingDark
